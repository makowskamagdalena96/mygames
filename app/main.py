from typing import Optional

from bson.errors import InvalidId
from fastapi import FastAPI

from app.config.config import MONGO_HOST, MONGO_PORT, MONGO_USER, MONGO_PASS, DB_NAME, DB_COL_NAME
from app.data_access.exceptions import ObjectNotExists
from app.data_access.games_repo import GamesRepo
from app.data_model.api import GamesResponse, GameResponse, UpdateGameRequest

app = FastAPI(
    title='myGames'
)

games_repo: Optional[GamesRepo] = None


async def get_games_repo():
    global games_repo
    games_repo = GamesRepo(MONGO_HOST, MONGO_PORT, MONGO_USER, MONGO_PASS, DB_NAME, DB_COL_NAME)


app.add_event_handler('startup', get_games_repo)


@app.get('/games/', response_model=GamesResponse)
async def get_all_games():
    """Returns all games"""
    all_games = await games_repo.get_all_games()
    return GamesResponse(games=[GameResponse(id=str(g['_id']), name=g['name'], price=g['price'], version=g.get('version'))
                                for g in all_games])


@app.post("/game/{game_id}")
async def get_game_details(game_id: str):
    """Returns game details"""
    try:
        game = await games_repo.find_game(game_id)
    except ObjectNotExists:
        return {"code": 404, "message": "Game doesn't exist"}
    except InvalidId:
        return {"code": 400, "message": "Game id is incorrect"}
    return GameResponse(id=str(game['_id']), name=game['name'], price=game['price'], version=game.get('version'))


@app.delete("/game/{game_id}")
async def delete_game(game_id: str):
    """Deletes game"""
    try:
        await games_repo.delete_game(game_id)
    except ObjectNotExists:
        return {"code": 404, "message": "Game doesn't exist"}
    except InvalidId:
        return {"code": 400, "message": "Game id is incorrect"}


@app.put("/game/{game_id}")
async def update_game(game_id: str, game: UpdateGameRequest):
    """Updates game"""
    data_to_update = {k: v for k, v in game.dict().items() if v is not None}
    try:
        update_successful = await games_repo.update_game(game_id, data_to_update)
        if not update_successful:
            return {"code": 40, "message": "Something went wrong ..."}
    except ObjectNotExists:
        return {"code": 404, "message": "Game doesn't exist"}
    except InvalidId:
        return {"code": 400, "message": "Game id is incorrect"}
    game = await games_repo.find_game(game_id)
    return GameResponse(id=str(game['_id']), name=game['name'], price=game['price'], version=game.get('version'))
