from typing import List, Dict

from bson import ObjectId
from motor import motor_asyncio

from app.data_access.exceptions import ObjectNotExists


class MongoWrapper:

    def __init__(self, host: str, port: int, user: str, password: str, db_name: str, db_col_name: str):
        client = motor_asyncio.AsyncIOMotorClient(f'mongodb://{host}:{port}', username=user, password=password)
        db = client[db_name]
        self.collection = db[db_col_name]

    async def insert(self, data: List[Dict]):
        result = await self.collection.insert_many(data)
        print(f'Inserted {len(result.inserted_ids)} docs')

    async def find_all(self) -> List[Dict]:
        cursor = self.collection.find()
        return await cursor.to_list(length=100)

    async def find_document(self, item_id: str) -> Dict:
        game = await self.collection.find_one({'_id': ObjectId(item_id)})
        if not game:
            raise ObjectNotExists
        return game

    async def delete_document(self, item_id: str):
        await self.find_document(item_id)
        await self.collection.delete_one({'_id': ObjectId(item_id)})

    async def update_document(self, item_id: str, values_to_change: Dict) -> bool:
        await self.find_document(item_id)
        updated = await self.collection.update_one({'_id': ObjectId(item_id)}, {'$set': values_to_change})
        if updated:
            return True
        return False
