from typing import List, Dict

from app.data_access.mongo_wrapper import MongoWrapper


class GamesRepo(MongoWrapper):

    def __init__(self, host: str, port: int, user: str, password: str, db_name: str, db_col_name: str):
        super().__init__(host, port, user, password, db_name, db_col_name)

    async def insert_games(self, data: List[Dict]):
        await self.insert(data=data)

    async def get_all_games(self) -> List[Dict]:
        return await self.find_all()

    async def find_game(self, game_id: str) -> Dict:
        return await self.find_document(item_id=game_id)

    async def delete_game(self, game_id: str):
        await self.delete_document(item_id=game_id)

    async def update_game(self, game_id: str, data: Dict) -> bool:
        return await self.update_document(item_id=game_id, values_to_change=data)
