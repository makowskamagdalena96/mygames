from typing import Optional, List

from pydantic.main import BaseModel


class GameResponse(BaseModel):

    id: str
    name: str
    price: float
    version: Optional[int] = None


class GamesResponse(BaseModel):

    games: List[GameResponse]


class UpdateGameRequest(BaseModel):

    name: Optional[str]
    price: Optional[float]
    version: Optional[int]
