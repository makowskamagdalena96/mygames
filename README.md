**Instruction**

Go to: <project_path>/myGames/docker

Run: built.bat

Go to: <project_path>/myGames

Run: docker-compose up -d

Run: docker exec -it app-demo python init/init_data.py

Application API (Swagger) is available from localhost:8080/docs

Enjoy!
---
