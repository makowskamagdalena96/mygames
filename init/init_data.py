import asyncio

from app.config.config import MONGO_HOST, MONGO_PORT, MONGO_USER, MONGO_PASS, DB_NAME, DB_COL_NAME
from app.data_access.games_repo import GamesRepo

initial_data = [{'name': f'Game {i + 1}', 'price': round(i * 10.33 + 0.35, 2), 'version': i % 4 + 1} for i in range(30)]
for index, game in enumerate(initial_data):
    if index % 3 == 0:
        game.pop('version')

games_repo = GamesRepo(MONGO_HOST, MONGO_PORT, MONGO_USER, MONGO_PASS, DB_NAME, DB_COL_NAME)

loop = asyncio.get_event_loop()
loop.run_until_complete(games_repo.insert_games(initial_data))
